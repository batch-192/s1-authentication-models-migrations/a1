@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Likes: {{count($post->likes)}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
						@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
				
			         {{-- COMMENTS --}}
			         @if(count($post->comments)>0)
			            <h5>Comments:</h5>
			            
			            @foreach($post->comments as $comment)
			            <div class="alert alert-secondary">
			               <p class="text-muted">{{$comment->user->name}}: {{$comment->created_at}}</p>
			               <h6>{{$comment->content}}</h6>       
			            </div>
			            @endforeach
			         @endif
				</form>
			@endif
			   
			{{-- MODAL  --}}
			<!-- Button trigger modal -->
			<form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">			
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
			  Comment
			</button>

			<!-- Modal -->
			<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			  	<form class="modal-content" action="/posts/{{$post->id}}/comment" method="POST">
			         @csrf
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="commentModalLabel">Comment</h5>
			        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			      </div>
			      <div class="modal-body">
			        <textarea name="content" rows="10" placeholder="Write comment here" class="w-100"></textarea>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary">Post Comment</button>
			      </div>
			    </div>
			    </form>
			  </div>
			</div>

			   <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModal" aria-hidden="true">
			      <div class="modal-dialog" role="document">
			        <form class="modal-content" action="/posts/{{$post->id}}/comment" method="POST">
			         @csrf
			          <div class="modal-header">
			            <h5 class="modal-title" id="commentModal">Comment</h5>
			            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			              <span aria-hidden="true">&times;</span>
			            </button>
			          </div>
			          <div class="modal-body">
			            <textarea name="content" rows="10" placeholder="Write comment here" class="w-100"></textarea>
			          </div>
			          <div class="modal-footer">
			            <button type="submit" class="btn btn-outline-primary border-0">Post comment</button>
			          </div>
			        </form>
			      </div>
			    </div>
			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>

	

@endsection
