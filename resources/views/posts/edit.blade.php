@extends('layouts.app')

@section('content')
    	<form method="POST" action="/posts/{{$post->id}}">
    		@method('PUT')
    	  	@csrf

	        <div class='form-group'>           
	            <label for="title-input">Title</label>
	            <input id="title-input" type="text" name="title" class="form-control" placeholder="Title" value="{{$post->title}}">
	        </div>

	        <div class='form-group'>
	            <label for="content-input">Content</label>
	            <textarea id="content-input" name="content" class="form-control" placeholder="Content" rows="3">{{$post->content}}</textarea>
	        </div>

	        <div class='form-group mt-2'>
	            <button type="submit" class="btn btn-danger px-5">Update Post</button>
	        </div>
   		</form>
  

@endsection
